<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Payment;
use Hash;

class PaymentController extends Controller
{
        /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $payment = New Payment;
        $payment->monto = $request->monto;
        $payment->descripcion = $request->descripcion;
        $payment->id_cliente = $request->cedula;
        $payment->fecha = $request->fecha;
        $payment->save();
        if($payment->save()){
            return 'ok';
        }else{
            return 'error';
        } 
    }
}
