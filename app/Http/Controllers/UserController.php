<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = New user;
        $user->name = $request->name;
        $user->last_name = $request->last_name;
        $user->phone = $request->phone;
        $user->cedula = $request->cedula;
        $user->password = isset($request->password) ? Hash::make($request->password):Hash::make($request->cedula);
        $user->tipo = $request->tipo;
        $user->save();
        if($user->save()){
            return 'ok';
        }else{
            return 'error';
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if(isset($request->cedula)){
            
            $user = user::where('cedula', '=', $request->cedula)->first();

            if($user != null){
                $user->name = $request->name ? $request->name:$user->name;
                $user->last_name = $request->last_name ? $request->last_name:$user->last_name;
                $user->phone = isset($request->phone) ? $request->phone:$user->phone;
                $user->cedula = $request->cedula;
                $user->password = isset($request->password) ? Hash::make($request->password):Hash::make($request->cedula);
                $user->tipo = $request->tipo ? $request->tipo:$user->tipo;
                $user->save();
                if($user->save()){
                    return 'Updated';
                }else{
                    return 'error';
                }
            }else{
                return 'Cedula no encontrada';
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function searchUserById(Request $request)
    {
        $user = user::findOrFail($request->id);

        return $user;
    }

    /**
     * Remove the specified resource from storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function deleteUserById(Request $request)
    {
        $user = user::findOrFail($request->id);

        $delete = $user->delete();
        
        if($delete==1){
            return 'Usuario eliminado';
        }else{
            return 'Usuario no eliminado, intente de nuevo';
        }
    }
}
