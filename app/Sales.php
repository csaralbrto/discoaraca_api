<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{
    protected $table = 'sales';
    protected $fillable = [
        'monto', 'kg', 'precio', 'fecha', 'descripcion', 'user_id',
    ];
}
