<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
/* Ruta para crear un usuario */
Route::post('saveUser', 'UserController@store')->name('saveUser');

/* Ruta para editar un usuario */
Route::post('updateUser', 'UserController@update')->name('updateUser');

/* Ruta para eliminar un usuario */
Route::post('deleteUser', 'UserController@deleteUserById')->name('deleteUser');

/* Ruta para traer un usuario */
Route::post('searchUser', 'UserController@searchUserById')->name('searchUser');

/* Ruta para agregar un abono */
Route::post('sale', 'SalesController@store')->name('sale');

/* Ruta para agregar un abono */
Route::post('payment', 'PaymentController@store')->name('payment');
